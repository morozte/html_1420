/* Primer ejemplo JS - 23/06/2020 */
// Camada 1420 TEL

/* Variables */

var numero = 7;
var pi = 3.1415;
var nombre = "Emanuel";

console.log(numero);
console.log(pi);
console.log(nombre);

let apellido = "Moroni";

console.log(apellido);

var sumatoria = numero + pi;

console.log(sumatoria);

var dato = nombre + " " + apellido;

console.log(dato);

/* Condicionales */

var a = 9;
var b = 10;

if (a > b)
{
    console.log("a es mayor que b");
}
else
{
    console.log("b es mayor o igual a la variable a");
}

var op_1 = 3;
var op_2 = 2; 
var resultado;
var operacion = "SUMA"; // 0 - suma ; 1 - resta ; 2 - multiplicación ; 3 - división

switch (operacion)
{
    case "SUMA":
        resultado = op_1 + op_2;
        break;
    case "RESTA":
        resultado = op_1 - op_2;
        break;
    case "MULTIPLICACION":
        resultado = op_1 * op_2;
        break;
    case "DIVISION":
        resultado = op_1 / op_2;
        break;
}

console.log("El resultado de la operación es: " + resultado);

/* Funciones */

function presentacion ()
{
    console.log("Mi primer ejemplo JS, XD");
}

presentacion();
